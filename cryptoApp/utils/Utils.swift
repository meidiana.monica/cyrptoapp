//
//  Utils.swift
//  cryptoApp
//
//  Created by Meidiana Monica on 24/11/22.
//

import Foundation

class Utils: NSObject, URLSessionWebSocketDelegate {
    class func formattedPrice(price: String) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        guard let price = Double(price), let formattedPrice = numberFormatter.string(from: NSNumber(value:price)) else { return "" }
        return formattedPrice
    }
}
