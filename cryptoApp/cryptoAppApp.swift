//
//  cryptoAppApp.swift
//  cryptoApp
//
//  Created by Meidiana Monica on 15/11/22.
//

import SwiftUI

@main
struct cryptoAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
