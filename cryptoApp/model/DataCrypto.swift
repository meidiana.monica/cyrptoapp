//
//  DataCrypto.swift
//  cryptoApp
//
//  Created by Meidiana Monica on 15/11/22.
//

import Foundation

struct DataCrypto: Decodable {
    let status: String
    let data: DataCoins
}

struct DataCoins: Decodable {
    let stats: Stats
    let coins: [Coins]
}

struct Stats: Decodable {
    let total: Int
    let totalCoins: Int
}

struct Coins: Decodable, Hashable {
    let symbol: String
    let name: String
    let rank: Int
    let iconUrl: String
    let price: String
}
