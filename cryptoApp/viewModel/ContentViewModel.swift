//
//  ContentViewModel.swift
//  cryptoApp
//
//  Created by Meidiana Monica on 15/11/22.
//

import Foundation
import Combine

class ContentViewModel: ObservableObject {
    @Published var coins = [Coins]()
    private var cancellables = Set<AnyCancellable>()
    
    var components: URLRequest {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "api.coinranking.com"
        components.path = "/v2/coins"
        
        var request = URLRequest(url: (components.url)!)
        request.httpMethod = "GET"
        request.setValue("x-access-token", forHTTPHeaderField: "coinranking53e17e97b705e14183df61f339506149b920781b1b02c74f")
        
        return request
    }
    
    func fetchCoins() {
        print("url component : \(self.components)")

        URLSession.shared
            .dataTaskPublisher(for: components)
            .map(\.data)
            .decode(type: DataCrypto.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                    case let .failure(error):
                        print(error.localizedDescription)
                default:
                    break
                }
            }, receiveValue: { value in
                print("value \(value)")
                self.coins = value.data.coins
                print("[ContentViewModel] coins : \(self.coins)")
            })
            .store(in: &self.cancellables)
    }
    
}
