//
//  ContentView.swift
//  cryptoApp
//
//  Created by Meidiana Monica on 15/11/22.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var contentVM = ContentViewModel()
    
    var body: some View {
        VStack {
            List {
                ForEach(contentVM.coins, id:\.self) { list in
                    HStack {
                        Text("\(list.rank)")
                        // searching to resolve image with ext svg seems not appear, with ext png will appear
                        AsyncImage(url: URL(string: list.iconUrl)) { image in
                            image
                                .resizable()
                                .aspectRatio(contentMode: .fill)

                        } placeholder: {
                            Color.gray
                        }
                        .frame(width: 50, height: 50)
                        
//                        SVGImageView(url: URL(string: list.iconUrl)!, size: CGSize(width: 50,height: 50))
                        Text(list.name)
                        
                        Spacer()
                        
                        Text("\(Utils.formattedPrice(price: list.price))")
                    }
                }
            }
        }.onAppear{
            contentVM.fetchCoins()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
